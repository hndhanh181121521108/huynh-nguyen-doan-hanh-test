*** Settings ***
Library           ../../Users/Admin/PycharmProjects/pythonProject/venv/KeywordHanh.py

*** Test Cases ***
TC01
    ${session}    OpenBrowser    https://demo.guru99.com/v4/
    clickElemXpath    ${session}    //button[@id="details-button"]
    clickElemXpath    ${session}    //a[@id="proceed-link"]
    clickElemName    ${session}    uid
    setValueByName    ${session}    uid    mngr340790
    clearInputXpath    ${session}    //input[@name='uid']
    verifyMessageId    ${session}    message23    User-ID must not be blank
    clickElemName    ${session}    password
    setValueByName    ${session}    password    amAtezy
    clearInputXpath    ${session}    //input[@name='password']
    verifyMessageId    ${session}    message18    Password must not be blank
    setValueByName    ${session}    uid    mngr340790
    setValueByName    ${session}    password    mngr340790
    clickElemXpath    ${session}    //input[@name='btnReset']
    setValueByName    ${session}    uid    mngr340790
    setValueByName    ${session}    password    amAtezy
    clickElemName    ${session}    btnLogin

TC02
    ${session}    OpenBrowser    https://demo.guru99.com/v4/
    clickElemXpath    ${session}    //button[@id="details-button"]
    clickElemXpath    ${session}    //a[@id="proceed-link"]
    login    ${session}    //input[@name='uid']    mngr340790    //input[@name="password"]    amAtezy    btnLogin
