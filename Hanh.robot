*** Settings ***
Documentation     Exercise 1: Verify the Login Section
...               - Go to https://demo.guru99.com/v4/
...               - Verify text 'User-ID must not be blank' and 'Password must not be blank' if userID or password field is empty
...               - Input userID and password
...               - Verify text 'User-ID must not be blank' and 'Password must not be blank' are disappeared
...               - Click Reset button
...               - Verify userID and password is cleared
...               - Input valid userID and valid password
...               - Click button login
...               - Verify login to home page successful
...               - Verify dynamic userID in home page
...               - Evidence screenshots are required for verification steps
Library           Selenium2Library

*** Test Cases ***
TC1
    Open Browser    https://demo.guru99.com/v4/    Chrome
    Click Button    //button[@id="details-button"]
    Click Link    //a[@id="proceed-link"]
    Click Element    //input[@name="uid"]
    Click Element    //td[text()="UserID"]
    ${result}    Get Text    //label[@id="message23"]
    Should Contain    ${result}    User-ID must not be blank
    Click Element    //input[@name="password"]
    Click Element    //td[text()="Password"]
    ${result}    Get Text    //label[@id="message18"]
    Should Contain    ${result}    Password must not be blank
    Input Text    //input[@name="uid"]    mngr340790
    Input Text    //input[@name="password"]    amAtezy
    Clear Element Text    //input[@name="uid"]
    ${result}    Get Text    //label[@id="message23"]
    Should Contain    ${result}    User-ID must not be blank
    Clear Element Text    //input[@name="password"]
    ${result}    Get Text    //label[@id="message18"]
    Should Contain    ${result}    Password must not be blank
    Click Button    //input[@name="btnReset"]
    Input Text    //input[@name="uid"]    mngr340790
    Input Text    //input[@name="password"]    amAtezy
    Click Button    //input[@name="btnLogin"]
    Location Should Be    https://demo.guru99.com/v4/manager/Managerhomepage.php
    Capture Page Screenshot    ${output_dir}/selenium-screenshot-1.png
    Close Browser
