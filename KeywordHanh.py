from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def OpenBrowser(url):
    driver = webdriver.Chrome()
    driver.get(url)
    return driver

# Click

def clickElemName(driver, name):
    driver.find_element_by_name(name).click()

def clickElemId(driver, id):
    driver.find_element_by_id(id).click()

def clickElemXpath(driver, xpath):
    driver.find_element_by_xpath(xpath).click()

# Get Value

def getValue(driver, xpath):
    value = driver.find_element_by_xpath(xpath).text
    return value

# Set Value

def setValueByName(driver, name, value):
    driver.find_element_by_name(name).send_keys(value)

def setValueById(driver, id, value):
    driver.find_element_by_id(id).send_keys(value)

def setValueByXpath(driver, xpath, value):
    driver.find_element_by_xpath(xpath).send_keys(value)


def locationShouldBe(url):
    driver = webdriver.Chrome()
    driver.get(url)
    return driver

def shouldContent(content1, content2):
    if content1 == content2:
        return True
    else:
        return False

def closeBrowser(driver):
    driver.close()
    driver.quit()

# Login

def login(driver, xpathUsername, username, xpathPass, password, submitBtn):
    clickElemXpath(driver, xpathUsername)
    setValueByXpath(driver, xpathUsername, username)
    clickElemXpath(driver, xpathPass)
    setValueByXpath(driver, xpathPass, password)
    clickElemName(driver, submitBtn)

# Verify Message

def verifyMessage(driver, xpath, value):
    result = driver.find_elements_by_xpath(xpath)[0].text
    assert result == value

def verifyMessageId(driver, id, message):
    element = driver.find_elements_by_id(id)[0].text
    assert element == message

# Clear text

def clearInputName(driver, name):
    driver.find_element_by_name(name).clear()

def clearInputId(driver, id):
    driver.find_element_by_id(id).clear()

def clearInputXpath(driver, xpath):
    driver.find_element_by_xpath(xpath).clear()